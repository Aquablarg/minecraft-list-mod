package aquaczaz.blocklist;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class CommonProxy {
	
	public static KeyBinding[] keyBindings;
	
	//preInitialization
    public void preInit(FMLPreInitializationEvent e) {

    }
    //Initialization
    public void init(FMLInitializationEvent e) {
    	
    	//register the GuiHandler
    	NetworkRegistry.INSTANCE.registerGuiHandler(Main.instance, new ModGuiHandler());
    	System.out.println("Gui Handler registered!");
    	//declare array of keyBindings, just one for now
    	keyBindings = new KeyBinding[1];
    	//initialize keyBindings
    	keyBindings[0] = new KeyBinding("key.List", Keyboard.KEY_L, "key.categories.mod");
    	//register keyBindings
    	for (int i = 0; i < keyBindings.length; ++i)
    	{
    		ClientRegistry.registerKeyBinding(keyBindings[i]);
    		System.out.println("registered key" + keyBindings[i].getKeyCode());
    	}
    	System.out.println("Key Bindings registered!");
    }
    
    //post Initialization
    public void postInit(FMLPostInitializationEvent e) {

    }
   

}
