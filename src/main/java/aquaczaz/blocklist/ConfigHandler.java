package aquaczaz.blocklist;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import aquaczaz.blocklist.GuiBlockList.GuiBlock;

import java.lang.String;

import net.minecraft.block.Block;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod.EventHandler;

public class ConfigHandler {
	
	private static final String ISCHECKED = "Is Checked";
	private static final String NAMES = "block names";
	
	public static Configuration configFile;
	
	@EventHandler
    public void Init(File config){
		
	configFile = new Configuration(config);
	syncConfig();
	}

    public static void syncConfig(){
    	try{
    		configFile.load();
    		Iterator itr = Block.REGISTRY.iterator();
    		int i = 0;
    		while(itr.hasNext()){
    		Block element = (Block) itr.next();
    		if (!configFile.getCategory("test category").containsKey(Integer.toString(i))){
    		Property isChecked = configFile.get(ISCHECKED,Integer.toString(i), false, element.getLocalizedName());
    		Property names = configFile.get(NAMES, Integer.toString(i), element.getLocalizedName());
    		System.out.println("added " + element.getLocalizedName() + " to config");
    		}
    		++i;
    	}
        		
    	} catch (Exception e){
    		
    	} finally {
    		if(configFile.hasChanged()) configFile.save();
    	}
    	
    }
    
    //returns size of category in configuration file
    public int getSize(){
    	return configFile.getCategory(NAMES).size();
    }
    
    //returns comment for key location in configuration file
    public String getName(int key){
    	return configFile.getCategory(NAMES).get(Integer.toString(key)).getString();
    }
    
    public static void saveCheckState(ArrayList<GuiBlock> guiblocks){
    	for (int i = 0; i < guiblocks.size(); i++){
    		configFile.getCategory(ISCHECKED).get(Integer.toString(i)).set(guiblocks.get(i).isPressed);
   
    	}
    	if(configFile.hasChanged()) configFile.save();
    	
    }
    
    public boolean getIsChecked(int idx)
    {
    	return configFile.getCategory(ISCHECKED).get(Integer.toString(idx)).getBoolean();
    }

}
