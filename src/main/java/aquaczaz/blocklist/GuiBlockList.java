package aquaczaz.blocklist;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiListExtended;
import net.minecraftforge.fml.client.config.*;

import java.util.*;

public class GuiBlockList extends GuiListExtended {

	public static ArrayList<GuiBlock> guiBlocks;
    private int maxListLabelWidth;
    private Minecraft mc;
    private ConfigHandler config = Main.configInstance;
	
	public GuiBlockList(GuiBlockChecklist checklist, Minecraft mcIn) {
		super(mcIn, checklist.width, checklist.height, 0, checklist.height, 20);
		String[] blocks = new String[config.getSize()];
		System.out.println(config.getSize());
		for (int i = 0; i < blocks.length; i++){
		blocks[i] = config.getName(i);
		
		}
		
		// Make a bunch of test GuiBlock objects - eventually read these from a list of known blocks?
		guiBlocks = new ArrayList<GuiBlock>();
		mc = mcIn;
		for(int i = 0; i < config.getSize(); i++)
		{
			guiBlocks.add(new GuiBlock(config.getName(i), config.getIsChecked(i), mcIn));
		}

	}

	@Override
	public IGuiListEntry getListEntry(int index) {
		return guiBlocks.get(index);
	}
	
	public int getNumBlocks()
	{
		return getSize();
	}
	
	@Override
	protected int getSize() {
		return config.getSize();
	}
	
    protected int getScrollBarX()
    {
        return super.getScrollBarX() + 35;
    }
	
	// This is the GuiBlock definition.  Each instance of this represents a block on this list
	public class GuiBlock implements IGuiListEntry
	{
		public String entryName;
		public Minecraft mc;
		public GuiButton theBlockToDraw;
		public boolean isPressed;
		
		public GuiBlock(String name, boolean aIsPressed, Minecraft mcIn)
		{
			entryName = name;
			mc = mcIn;
			isPressed = aIsPressed;
		}
		
		@Override
		public void setSelected(int p_178011_1_, int p_178011_2_,
				int p_178011_3_) {
			// TODO Auto-generated method stub
			
		}

		// This shoulc get called for each GuiBlock contained in the GuiBlockList
		@Override
		public void drawEntry(int slotIndex, int x, int y, int listWidth,
				int slotHeight, int mouseX, int mouseY, boolean isSelected) 
		{
			theBlockToDraw = new GuiCheckBox(slotIndex, x, y, entryName, isPressed);
			theBlockToDraw.drawButton(mc, mouseX, mouseY);
		}

		@Override
		public boolean mousePressed(int slotIndex, int mouseX, int mouseY,
				int mouseEvent, int relativeX, int relativeY) {
			isPressed = !isPressed;
			return false;
		}

		@Override
		public void mouseReleased(int slotIndex, int x, int y, int mouseEvent,
				int relativeX, int relativeY) {
			theBlockToDraw.mouseReleased(mouseX, mouseY);
		}
		
	}

}
