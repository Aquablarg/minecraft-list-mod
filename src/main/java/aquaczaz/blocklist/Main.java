package aquaczaz.blocklist;

import java.util.Iterator;

import net.minecraft.block.*;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.VERSION)
public class Main {

    public static final String MODID = "tutorial";
    public static final String MODNAME = "Tutorial Mod";
    public static final String VERSION = "1.0.0";
    
    public static boolean[] boxList;
    
    //create proxy and config calls for initialization
    @SidedProxy(clientSide="aquaczaz.blocklist.ClientProxy", serverSide="aquaczaz.blocklist.ClientProxy")
    public static CommonProxy proxy;
    public static ConfigHandler configInstance = new ConfigHandler();
    
    //create instance of Main()
    @Instance
    public static Main instance = new Main();
   
    
    
    //mod preinitialization
    @EventHandler
    public void preInit(FMLPreInitializationEvent e) {
    	System.out.println("This part is working");
        proxy.preInit(e);
        configInstance.Init(e.getSuggestedConfigurationFile());
        
    }
    
    //mod Initialization
    @EventHandler
    public void init(FMLInitializationEvent e) {
    	System.out.println("This part is working");
    	proxy.init(e);
    	
    	System.out.println("register event listener");
    	MinecraftForge.EVENT_BUS.register(new EventWatDo());
    }
        
    //mod post initialization
    @EventHandler
    public void postInit(FMLPostInitializationEvent e) {
    	System.out.println("This part is working");
    	proxy.postInit(e);
    }
    

}