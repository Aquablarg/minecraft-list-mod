package aquaczaz.blocklist;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class ModGuiHandler  implements IGuiHandler {
	
	public static final int MOD_BLOCK_LIST_GUI = 0;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
    	if (ID == MOD_BLOCK_LIST_GUI)
        	return new GuiBlockChecklist();
    	
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == MOD_BLOCK_LIST_GUI)
        	return new GuiBlockChecklist();
        
        return null;
    }
    
}
